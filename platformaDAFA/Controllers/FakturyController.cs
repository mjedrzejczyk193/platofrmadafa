﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace platformaDAFA.Controllers
{
    public class FakturyController : Controller
    {
        // GET: Faktury
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DodajPozycjeFaktury(int idsprzedazy, string nazwa, string pkwiu, string jm, int ilosc, float netto, float wnetto, float wvat, float wbrutto, int ids, int idps, int kmd, string vat, int mat)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.
        ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    conn.Open();

                    // 1.  create a command object identifying the stored procedure
                    SqlCommand cmd = new SqlCommand("dbo.pozycjeSprzedazOperacje", conn);

                    // 2. set the command object so it knows to execute a stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    // 3. add parameter to command, which will be passed to the stored procedure
                    cmd.Parameters.Add(new SqlParameter("@idSprzedazy", idsprzedazy));
                    cmd.Parameters.Add(new SqlParameter("@nazwaTU", nazwa));
                    cmd.Parameters.Add(new SqlParameter("@PKWiU", pkwiu));
                    cmd.Parameters.Add(new SqlParameter("@jm", jm));
                    cmd.Parameters.Add(new SqlParameter("@ilosc", ilosc));
                    cmd.Parameters.Add(new SqlParameter("@cenaNetto", netto));
                    cmd.Parameters.Add(new SqlParameter("@wartNetto", wnetto));
                    cmd.Parameters.Add(new SqlParameter("@wartVat", wvat));
                    cmd.Parameters.Add(new SqlParameter("@wartBrutto", wbrutto));
                    cmd.Parameters.Add(new SqlParameter("@idSesji", ids));
                    cmd.Parameters.Add(new SqlParameter("@komendaPozycje", kmd));
                    cmd.Parameters.Add(new SqlParameter("@idPozycjiSprzedaz", idps));
                    cmd.Parameters.Add(new SqlParameter("@vat", vat));
                    cmd.Parameters.Add(new SqlParameter("@materialyuslugislownik", mat));

                    // execute the command
                    cmd.ExecuteNonQuery();
                }
            }
            catch(Exception e)
            {

            }

            return Redirect(string.Format("http://platforma.dafa.com.pl/aplikacje/602/faktury/fvDodaj.asp?idSprzedazy={0}", idsprzedazy));
        }

    }
}