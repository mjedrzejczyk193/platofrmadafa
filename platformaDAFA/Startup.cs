﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(platformaDAFA.Startup))]
namespace platformaDAFA
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
